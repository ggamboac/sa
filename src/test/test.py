# src/test/test.py
import os
import unittest
from src.python_cliente import  crear_cliente_test
class TestMyModule(unittest.TestCase):
    

    def test_creacion_cliente(self):
        print("Prueba unitaria creacion de Cliente: ")
        print(crear_cliente_test(13,"Cliente Prueba","Apellido Prueba"))
        self.assertEqual(crear_cliente_test(13,"Cliente Prueba","Apellido Prueba"), '{"nombre": "Cliente Prueba", "number": 13, "apellido": "Apellido Prueba"}')        
if __name__ == "__main__":
    unittest.main()